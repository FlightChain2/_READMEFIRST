# SITA AVIATION BLOCKCHAIN SANDBOX

The SITA Aviation Blockchain Sandbox was created to foster collaboration relating to the application of blockchain technologies in the air transport industry by adopting open access, best practices in research and transparency

### RESEARCH PRINCIPLES
The Aviation Blockchain Sandbox is a development and test platform, adopting best practices in research and applying minimal data usage to each specific use case. The principles we follow are:

* Identify a clear benefit for air transport efficiency and improvement
* Adopt research methods and tools which have the minimum intrusion
* Create robust methods
* Adopt transparency in research with data providers
* Apply the relevant security practices based on a risk assessment on a use case by use case basis

### OBJECTIVES
The core objectives for the Aviation Blockchain Sandbox are to provide:
* An air transport industry-wide problem-solving Sandbox for collaboration on blockchain projects to accelerate innovation and research
* Open accessibility to promote knowledge sharing in blockchain and enabling technologies
* Shared observations and outputs and informed approaches for blockchain adoption in the air transport industr 

### CATEGORIES OF PARTICIPANTS
The SITA Aviation Blockchain Sandbox is open for participation to companies working in the air transport industry and who have a shared interest in blockchain technologies. In the first phase of the SITA Aviation Sandbox, all participants will be Contributors. In future phases participants may participate as Users, Sponsors or Observers as well as Contributors. Different roles may include different permissions and access to projects and data as determined to be appropriate to the project.

* **Users** are those who use data or who manage nodes within a specific Contributor’s environment to conduct transactions.
* **Contributors** are the participating airlines and airports or their designated technology partners who subscribe to the Aviation Blockchain Sandbox and supply data and actively participate in the development and research of blockchain projects.
* **Sponsors** are the organizations who fund the project or are an air industry member sponsoring the activity of a non-ATI member’s participation in the project.
* **Observers** are the regulatory bodies with a role of oversight and who are not active Contributors or Users.

### PROJECTS
To ensure transparency of operation and to foster collaboration, prior to the commencement of each project, the project participants will agree the following (Project Criteria):

* Scope of work and project plan
* KPIs, targets, goals, learning objectives and other success criteria
* Named participants and project contacts
* Capacity planning requirements
* Whether the project is funded or non-funded
* Data sets to be used

A lead participant will be nominated to manage the project in line with the Project Criteria and will be responsible for publishing the Project Criteria, changes to the Project Criteria and project end.

Project participants will respect confidentiality of certain types of data, Therefore, in certain use-cases, SITA will create a separate project workspace for contributors to ensure data remains confidential.

All Project Criteria will be published for all project participants to see. Any participant can request the relevant lead participant of a project to join their project. It is up to the participants to agree the members of each project.

Once the Project Criteria are published, the access to SITA Aviation Sandbox will be made available to designated project participants for the designated project duration.

At the end of the project the participants may agree to publish findings and share them with the SITA Aviation Sandbox community.


### FUNDING
Each participant will fund their own activities as agreed in the Project Criteria prior to the start of any project. If a particular project requires more resources from a participant, then the project participants will agree funding arrangements.

### DATA
The SITA Aviation Blockchain Sandbox adopts the following safeguards for data (Data Principles):

* No real personally identifiable information is used or shared for projects. Synthetic data may be used
* No commercially sensitive data is used or shared between participants
* No credit card data or PCI (payment card industry) data is used or shared between participants

### SECURITY AND ACCESS CONTROLS
The SITA Aviation Blockchain Sandbox adopts the following technical practices for maintaining security and access controls (Security Principles):

* To participate in the Aviation Blockchain Sandbox, an individual must complete an access request form
* Access to systems is controlled by credentials issued to specific individuals within an organization
* Data is not encrypted by default (test system data).but can be if required for a specific use-case
* Workspaces or channels provide shared access to all Contributors to support collaboration and learning across all parties in an ecosystem, supply chain; however designated workspaces can be created to support specific project needs

To ensure the security of the Aviation Sandbox, in rare cases SITA may need to close projects and will do so in consultation with the project participants. However, if the Data Principles, Security Principles Confidentiality, Competition Law or Code of Conduct are not complied with in a project SITA may need to take immediate action to close a project.

### PERFORMANCE
SITA provides a shared infrastructure for the Aviation Sandbox on a “as is” basis, however, data is partitioned into sub-groups for collaboration for each project. There will be multiple projects running on the SITA Aviation Sandbox and capacity needs to be restricted to support functional development. Therefore, the following practices will be adopted for all projects (Performance Principles):

* No load testing
* No reverse engineering

### SUPPORT

SITA Lab provides support for the Aviation Blockchain Sandbox, based on best efforts for support and problem resolution, Monday – Friday, UK business hours. Contact via email, Blockchain.Sandbox@SITA.aero

A response will be provided within 2 business days.

### CONFIDENTIALITY
Participants will have access to other participants’ technical and operational information relevant for the adoption of blockchain into air transport use cases. Each participant only provides data and information for use in a defined project as stated in the Project Criteria.

Data provided by participants for projects is provided for free to the project participants for the purposes of the project only and for and no other purpose.

Commercial and confidential data should not be disclosed on the SITA Aviation Sandbox.

Each participant is responsible for the protection of their own commercially sensitive technical information and for compliance with the requirements of competition law (see below). Participants shall not disclose any other commercially sensitive information, in particular, participants shall not disclose any information which relates to prices, costs, marketing strategy or revenue streams.

The detailed substance of projects, all written outputs and those of its working parties are to be kept confidential between participants of a project.

SITA shall be entitled, at its discretion, to:

* Publish the names of current participants
* Prepare high level summaries of projects for open publication. Such summaries shall not identify the individual views or contributions of specific organizations by name, unless specifically authorized in writing to do so by the affected parties
* Use the information derived from the Aviation Sandbox activity freely for the onward development of the Aviation Sandbox
* Publish these terms of reference and current projects and any associated working parties

Participants shall be entitled, at their discretion, to:

* Make known their participation in the SITA Aviation Sandbox
* Withhold information from the SITA Aviation Sandbox which they deem to be commercially sensitive or otherwise private, even if it is directly relevant to the project objective, and participants must do so if any disclosure would risk breaching the competition compliance requirements set out below.

### COMPETITION LAW
The basic principle of competition law is that companies should act independently of each other and each should develop its own commercial strategy without discussing, agreeing or coordinating it with a competitor. To do otherwise or for competitors to pool their commercial knowledge may, and in most circumstances will, result in a breach of competition law.

However, in certain circumstances, where competitors pool their knowledge and expertise to seek an optimal technical solution or to achieve interoperability or efficiency for a product or solution, such activities will not be anti-competitive, provided that such knowledge sharing activities are not used for the exchange of information relating to any other commercial purpose.

The objectives of the SITA Aviation Sandbox are focused exclusively in seeking efficiency-enhancing, technical solutions for the application of blockchain in the air transport industry. Consequently, in order to comply with competition law, it is essential for all participants to ensure that discussions entered into (whether in any project meeting, working group meeting or in any informal discussions) do not involve the exchange of sensitive commercial information which is outside the scope of the SITA Aviation Sandbox objectives. In particular, discussions, or information exchanges, must not include any of the following subjects:

* **Price or service-related terms**: passenger fares; cargo rates; service fees (including a carrier’s ancillary fees); discounts; credit terms; warranty terms; refund policies; claims policies; travel agents’ commissions; frequent flyer program policies; methods of recouping costs, taxes or fees (including ticket or waybill surcharges to recoup government taxes or fees), alternative revenue streams, business models or service terms
* **Division or allocation of markets or customers**: limiting the geographic availability of a service or product, dividing up the territory in which a service or product will be provided, or dividing up customers
* **Customers or marketing information sharing**: principal target markets, types of customers or customer or marketing plans, proposals or strategies
* **Boycotts or blacklists**: limiting or refusing to do business with a customer, group of customers or category of customers, or a supplier, group of suppliers or category of suppliers
* **Requiring suppliers to use certain standards or specifications; or agreeing on which suppliers to use**: for example, requiring suppliers to use a particular technical specification or the specification of another organization, or specifying which suppliers to use (all parties must make such choices independently)
* **Competitively sensitive internal information** such as pricing, yield or capacity data, or marketing or service plans

Contract bids or requests for proposals: whether involving the government or a private entity

### CODE OF CONDUCT
The Aviation Blockchain Sandbox is an open-source and open community project where participants choose to work together, and in that process experience differences in language, location, nationality, and experience. In such a diverse environment, misunderstandings and disagreements happen, which in most cases can be resolved informally. In rare cases, however, behavior can intimidate, harass, or otherwise disrupt one or more people in the community, which the community will not tolerate.

A Code of Conduct is useful to define accepted and acceptable behaviors and to promote high standards of professional practice. It also provides a benchmark for self-evaluation and acts as a vehicle for better identity of the organization.

This Code of Conduct applies to any participant of the Aviation Blockchain Sandbox project community – developers, participants in meetings, teleconferences, mailing lists, conferences or functions, etc. Note that this code complements rather than replaces legal rights and obligations pertaining to any particular situation.

The Aviation Blockchain Sandbox project is committed to maintain a positive work environment. This commitment calls for a workplace where participants at all levels behave according to the rules of the following code. A foundational concept of this code is that we all share responsibility for our work environment.

Code

1. Treat each other with respect, professionalism, fairness, and sensitivity to our many differences and strengths, including in situations of high pressure and urgency
2. Never harass or bully anyone verbally, physically or sexually
3. Never discriminate on the basis of personal characteristics or group membership
4. Communicate constructively and avoid demeaning or insulting behavior or language
5. Seek, accept, and offer objective work criticism, and acknowledge properly the contributions of others.
6. Be honest about your own qualifications, and about any circumstances that might lead to conflicts of interest
7. Respect the privacy of others and the confidentiality of data you access
8. With respect to cultural differences, be conservative in what you do and liberal in what you accept from others, but not to the point of accepting disrespectful, unprofessional or unfair or unwelcome behavior or advances.
9. Promote the rules of this Code of Conduct and take action (especially if you are in a leadership position) to bring the discussion back to a more civil level whenever inappropriate behaviors are observed.
10. Stay on topic: Make sure that you are posting to the correct channel and avoid off-topic discussions. Remember when you update an issue or respond to an email you are potentially sending to a large number of people.
11. Step down considerately: Participants of every project come and go, and the Aviation Blockchain Sandbox is no different. When you leave or disengage from the project, in whole or in part, we ask that you do so in a way that minimizes disruption to the project. This means you should tell people you are leaving and take the proper steps to ensure that others can pick up where you left off.

### CREDITS
This code is based on the [Hyperledger Code of Conduct](https://wiki.hyperledger.org/community/hyperledger-project-code-of-conduct), [W3C’s Code of Ethics and Professional Conduct](https://www.w3.org/Consortium/cepc/) with some additions from the [Cloud Foundry‘s Code of Conduct](https://www.cloudfoundry.org/).

### INCIDENTS
To report incidents or to appeal reports of incidents, send email to Kevin O'Sullivan (kevin.osullivan@sita.aero) or Sherry Stein (sherry.stein@sita.aero). Please include any available relevant information, including links to any publicly accessible material relating to the matter. Every effort will be taken to ensure a safe and collegial environment in which to collaborate on matters relating to the Project. In order to protect the community, SITA reserves the right to take appropriate action, potentially including the removal of an individual from any and all participation in the project. SITA will work towards an equitable resolution in the event of a misunderstanding.



